# CDS Programming Challenge

## Challenge 1
The first programming challenge, fixing the 4 code examples, can be found in the "Challenge1.txt" text file in the "Challenge1" folder.


## Challenge 2
The second programming challenge, building the one page form, can be found in the "Challenge2" folder.

To view the completed form, open the "index.html" file in any browser after cloning the project:

```
git clone https://gitlab.com/tnrusso/programming-challenge.git
```
