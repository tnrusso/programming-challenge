function isNumber(event) {
    // Check to see if a key is an integer
    let key = event.keyCode
    if (key > 31 && (key < 48 || key > 57 && key <= 96) || (key <= 126 && key >= 123)) {
        return false;
    }
    return true;
}

function isLetter(event) {
    // Check to see if a key is a letter
    let key = event.keyCode
    if (key < 65 || key > 122 || (key >= 91 && key <= 96)) {
        return false;
    }
    return true;
}

function validateFields() {
    // Validating that all required fields are filled in
    let firstName = document.getElementById("firstName");
    let lastName = document.getElementById("lastName");
    let phoneNumber = document.getElementById("phoneNumber");
    let promoCode = document.getElementById("promoCode");
    let referredFrom = document.getElementById("referredFrom");
    let other = document.getElementById("other");
    let termsAndConditions = document.getElementById("termsAndConditions");

    let completedLabel = document.getElementById("completed-label");

    // Functions to show/hide the green 'completed' box
    function showLabel() {
        completedLabel.classList.remove("hidden")
        completedLabel.classList.add("visible")
        return true;
    }

    function hideLabel() {
        completedLabel.classList.add("hidden")
        completedLabel.classList.remove("visible")
        return false;
    }

    // First name, last name, phone number, email, and accepting the terms and conditions are always required
    if (firstName.value && lastName.value && phoneNumber.value && validateEmail() && termsAndConditions.checked) {
        if (referredFrom.value) {
            // If the user selects 'other' for 'How did you hear', then they are required to fill in the 'How did you hear: Other' input
            if (referredFrom.value == 'other') {
                if (other.value) {
                    return showLabel();
                } else {
                    return hideLabel();
                }
            } else {
                return showLabel();
            }
        } else {
            // If the user did not select anything for 'How did you hear', then they are required to enter a promo code
            if (promoCode.value) {
                return showLabel();
            } else {
                return hideLabel();
            }
        }
    } else {
        return hideLabel();
    }
}

function validateEmail() {
    // Check to make sure the user entered a valid email, Regular Expression taken from - https://stackoverflow.com/a/46181
    let email = document.getElementById("email");
    let regexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email.value) {
        return String(email.value).toLowerCase().match(regexEmail);
    } else {
        return false;
    }
}

function onPromoCodeChange(event) {
    // Validate promo code : 'How did you hear' is required if no promo code is entered
    let promoCode = document.getElementById("promoCode");
    let referredFromLabel = document.getElementById("referredFromLabel")

    if (promoCode.value == "") {
        referredFromLabel.classList.add("required");
        referredFromLabel.innerHTML = "How did you hear:*"
    } else {
        referredFromLabel.classList.remove("required");
        referredFromLabel.innerHTML = "How did you hear:"
    }
}

function onSelectChange() {
    // Hide the 'other' text field if the user does not select 'other', otherwise show it and require an input
    let referredFrom = document.getElementById("referredFrom").value;
    let otherRow = document.getElementById("other-row");

    if (referredFrom == 'other') {
        otherRow.style.display = 'block';
    } else {
        otherRow.style.display = 'none';
    }
    validateFields();
}

function openModal() {
    var modal = document.getElementById("modal");
    modal.style.display = "block";
}

function closeModal() {
    var modal = document.getElementById("modal");
    modal.style.display = "none";
}


window.onload = function () {
    var modal = document.getElementById("modal");
    let required = document.getElementsByClassName("required");

    // Add an asterisk (*) after every required field
    for (let i = 0; i < required.length; i++) {
        required[i].innerHTML += "*";
    }

    window.onclick = function (event) {
        // Close the modal when the user clicks outside of the modal content
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    window.onkeyup = function (event) {
        // Validate input fields when a user enters a key to trigger the 'completed' message
        validateFields();
    }
}
